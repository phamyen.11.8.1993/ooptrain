package QuanLyBanHang;

import java.util.ArrayList;

public class Application {

	public static void main(String[] args) {
//		SanPham sp = new SanPham();
//		sp.setName("nuoc chanh");
//		sp.setDessciption("chua");
//		sp.setPrice(7.00);
//		ArrayList<Integer> temp1 = new ArrayList<Integer>();
//		temp1.add(1);
//		temp1.add(2);
//
//		// ArrayList<Integer> temp1 = new ArrayList<Integer>(Arrays.asList(1,2));
//
//		sp.setRate(temp1);
//
//		ArrayList<Integer> temp = new ArrayList<>();
//		temp.add(5);
//		temp.add(4);
//
//		SanPham sp1 = new SanPham("nuoc cam", "chua", 9.07, temp);
//		sp.viewInfo();
//		sp1.inputInfo();
//		sp1.viewInfo();

//		FileUtils file = new FileUtils();
//		String fileContent = file.readFile("SanPham.txt");
//		System.out.println(fileContent);

		SanPham sp = CommonUtils.importSanPham("SanPham.txt");
		sp.viewInfo();
		System.out.println("---------------------");
		ArrayList<SanPham> tempList = CommonUtils.importSanPhams("SanPhams.txt");
		
		Shop ch = new Shop();
		ch.setProductList(tempList);
		
		ArrayList<SanPham> sanPhamList = ch.getProductList();

//		sanPhamList.forEach(spItem -> {
//			spItem.viewInfo();
//		});
//		sanPhamList.forEach(SanPham::viewInfo);
		// dung for
		for (int i = 0; i<sanPhamList.size();i++) {
			SanPham spTemp = sanPhamList.get(i);
			spTemp.viewInfo();
		}
		
	}

}
