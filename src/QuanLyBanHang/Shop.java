package QuanLyBanHang;

import java.util.ArrayList;

public class Shop {
	private String name;
	private String address;

//	·ArrayList ProductList// lưu danh sách các sản phẩm của shop
//
//	·void addProduct()// yêu cầu người dùng nhập thông tin của sản phẩm rồi lưu vàoProductList
//
//	·void removeProduct()// yêu cầu người dùng nhập vào tên sản phẩm sau đó tìm và xóa sản phẩm có tên tương ứng trong ProductList
//
//	·void iterateProductList()// hiển thị các sản phẩm trongProductList, gọi phương thức viewInfo() của lớpProduct, tính trung bình cộng đánh giá cho từng sản phẩm và hiển thị thông tin ra màn hình.
//
//	·void searchProduct()// yêu cầu người dùng nhập vào 2 số, sau đó tìm và hiển thị thông tin của những sản phẩm có giá nằm giữa hai số đó.
	private ArrayList<SanPham> productList;

	public ArrayList<SanPham> getProductList() {
		return productList;
	}

	public void setProductList(ArrayList<SanPham> productList) {
		this.productList = productList;
	}

	public Shop(ArrayList<SanPham> productList) {
		super();
		this.productList = productList;
	}
	
	public Shop() {
		super();
	}
	
	
}
