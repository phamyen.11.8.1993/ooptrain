package QuanLyBanHang;

import java.util.ArrayList;
import java.util.Scanner;

public class SanPham {
	/**
	 * String Name
	 * 
	 * ·String Description
	 * 
	 * ·double Price// 0 < Price <= 100
	 * 
	 * ·list<int> rate// lưu các đánh giá của người dùng cho sản phẩm, giá trị từ 1
	 * -5
	 */
	// primitive : int,long,....
	// reference: custom class
	private String name;
	private String dessciption;
	private double price;
	private ArrayList<Integer> rate = null;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDessciption() {
		return dessciption;
	}
	public void setDessciption(String dessciption) {
		this.dessciption = dessciption;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public ArrayList<Integer> getRate() {
		return rate;
	}
	public void setRate(ArrayList<Integer> rate) {
		this.rate = rate;
	}
	public SanPham(String name, String dessciption, double price, ArrayList<Integer> rate) {
		super();
		this.name = name;
		this.dessciption = dessciption;
		this.price = price;
		this.rate = rate;
	}
	
	public SanPham() {
		super();
	}
	
	private double computeRate() {
		double temp = 0;
		if (rate == null) {
			return 0;
		}
		for (int i = 0; i< rate.size();i++) {
			temp = temp + rate.get(i);
		}
		return temp / rate.size();
	}

	/**
	 * ·void viewInfo()// hiển thị tên, giá và mô tả về sản phẩm
	 */
	public void viewInfo() {
		System.out.println("name : " + this.name);
		System.out.println("description : " + this.dessciption);
		System.out.println("price : " + this.price);
		System.out.println("rate : " + this.computeRate());
		System.out.println("====");

	}

	/**
	 * Scanner
	 * ·void inputInfo()// input, giá và mô tả về sản phẩm
	 */ 
	public void inputInfo() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Nhap ten");
		this.name = scan.nextLine();
		System.out.println("Nhap mo ta");
		this.dessciption = scan.nextLine();
		System.out.println("Nhap gia");
		this.price = Double.parseDouble(scan.nextLine());
		System.out.println("Nhap rate");
		// 4.5
		// this.rate.add(4.5); 4,5 
		this.rate = new ArrayList<>();// 
		this.rate.add(Integer.parseInt(scan.nextLine()));

		
	}
}
