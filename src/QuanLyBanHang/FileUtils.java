package QuanLyBanHang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class FileUtils {

	public String readFile(String fileName) {
		InputStream is = getFileFromResourceAsStream(fileName);
		return getInputStream(is);
	}

	// get a file from the resources folder
	// works everywhere, IDEA, unit test and JAR file.
	private InputStream getFileFromResourceAsStream(String fileName) {

		// The class loader that loaded the class
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(fileName);

		// the stream holding the file content
		if (inputStream == null) {
			throw new IllegalArgumentException("file not found! " + fileName);
		} else {
			return inputStream;
		}

	}

	// print input stream
	private String getInputStream(InputStream is) {
		try (InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
				BufferedReader reader = new BufferedReader(streamReader)) {
			StringBuilder str = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				str.append(line).append("\n");
			}
			return str.toString().substring(0, str.length() - 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
}
