package QuanLyBanHang;

import java.util.ArrayList;

public class CommonUtils {
	/**
	 * import sanpham.txt
	 * @param fileName fileName
	 * @return SanPham
	 */
	public static SanPham importSanPham(String fileName) {
		// read file content
		FileUtils file = new FileUtils();
		String fileContent = file.readFile(fileName);
		String[] arrays = fileContent.split(",");
	
		// create san pham	
		SanPham sp = new SanPham();
		sp.setName(arrays[0]);
		sp.setDessciption(arrays[1]);
		sp.setPrice (Double.parseDouble(arrays[2]));

		ArrayList<Integer> temp = new ArrayList<>();
		temp.add(Integer.parseInt(arrays[3]));
		sp.setRate(temp);
		
		return sp;
	}
	
	public static ArrayList<SanPham> importSanPhams(String fileName) {
		// read file content
		ArrayList<SanPham> productList = new ArrayList<>();
		FileUtils file = new FileUtils();
		String fileContent = file.readFile(fileName);
		System.out.println(fileContent);
		String[] productArrs = fileContent.split("\n"); // \n
		
		for (int i=0; i<productArrs.length;i++) {
			String product = productArrs[i];
			String[] arrays = product.split(",");
			SanPham sp = new SanPham();
			sp.setName(arrays[0]);
			sp.setDessciption(arrays[1]);
			sp.setPrice (Double.parseDouble(arrays[2]));

			ArrayList<Integer> temp = new ArrayList<>();
			temp.add(Integer.parseInt(arrays[3]));
			sp.setRate(temp);
			productList.add(sp);
		}
		
		return productList;
	}
}
