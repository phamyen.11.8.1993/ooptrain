package da_hinh.overload;

public class OverloadingExample {
	// public: dễ dãi -> ai cũng có thể truy cập dc
	// default: cùng package 
	// protected: khác package, nhìn thấy ở con 
	// private : trong class ( nội bộ)

	public static long add(int a, int b) {
		return a + b;
	}

	public static long add(long a, long b) {
		return a + b;
	}

	public static long add(String a, String b) {
		return Long.parseLong( a) + Long.parseLong(b) ;
	}

	// khong dc phep
//	public static double add(long a, long b) {
//		return a + b;
//	}

	public static int add(int a, int b, int c) {
		return a + b + c;
	}
}
