package da_hinh;

import da_hinh.overload.OverloadingExample;
import da_hinh.override.Animal;
import da_hinh.override.DiCho;
import truu_tuong.Bike;
import truu_tuong.ICar;
import truu_tuong.ImplementCar;
import truu_tuong.SupBike;

public class Application {
	public static void main(String[] args) {
		A a = new B();
		
		A b = new C();
		// override
		System.out.println();
		
		OverloadingExample  overloadingExample = new OverloadingExample();
		overloadingExample.add(1, 2);
		
		overloadingExample.add(1l, 2l);
		
		overloadingExample.add(1,2,5);
		
//		Animal animal = new Animal();
//		animal.eat();
//		
//
//		DiCho dicho = new DiCho();
//		dicho.eat();
		
//		Animal animal1 = new DiCho();
//		animal1.eat();
		Bike sup = new SupBike();
		ICar i = new ImplementCar();
	}
}
