package vi_du_animal;

import java.util.ArrayList;

public class Application {
	public static void main(String[] args) {
		Animal cat = new Cat();
		Animal cat1 = new Cat();

//		cat.tiengKeu();

		Animal tiger = new Tiger();
//		tiger.tiengKeu();

		Animal dog = new Dog();
//		dog.tiengKeu();

		Zoo zoo = new Zoo();
		ArrayList<Animal> list = zoo.getAnimals();
		list.add(cat);
		list.add(cat1);
		list.add(tiger);
		list.add(dog);

		for (int i = 0; i < list.size(); i++) {
			Animal tempAnimal = list.get(i);
			tempAnimal.tiengKeu();
		}
	}
}
