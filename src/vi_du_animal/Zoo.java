package vi_du_animal;

import java.util.ArrayList;

public class Zoo {
	ArrayList<Animal> animals = new ArrayList<>();

	public ArrayList<Animal> getAnimals() {
		return animals;
	}

	public void setAnimals(ArrayList<Animal> animals) {
		this.animals = animals;
	}
	
}
