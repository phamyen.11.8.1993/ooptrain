package vi_du_animal;
//Bài tập 1: HỆ THỐNG QUẢN LÝ SỞ THÚ							
//1. Tạo lớp có tên Animal gồm các thuộc tính và phương thức:							
//· String Ten							
//· int Tuoi							
//· String MoTa							
//· void xemThongTin() //hiển thị loại, tên, tuổi và mô tả của động vật							
//· abstract void tiengKeu()							
//							
//2. Tạo một số hàm tạo cho lớp Animal như sau:							
//· 0 tham số							
//· 1 tham số (Ten),							
//· 2 tham số (Ten, Tuoi),							
//· 3 tham số (Ten, Tuoi, MoTa).							
//							
//3. Tạo các lớp Tiger, Dog, Cat theo các yêu cầu sau:							
//	Thừa kế từ lớp Animal						
//	Ghi đè phương thức tiengKeu() để thể hiện những tiếng kêu đặc trưng của từng loài vật						
//	Thực thi các hàm tạo sử dụng từ khóa super						
//							
//4. Tạo lớp có tên Zoo gồm:							Phần đỏ mình làm ở bài phần đa hình nhé chị
//· int maChuong							
//· ArrayList AnimalList							
//· void themConVat(Animal a) //thêm một con vật vào AnimalList							
//· void xoaConVat(String ten) //xóa con vật có tên tương ứng khỏi AnimalList				s			
public abstract class Animal {
	private String ten;
	private String moTa;

	public void xemThongTin() {
		System.out.println(this.ten);
		System.out.println(this.moTa);
	}

	public abstract void tiengKeu();

	public Animal() {

	}
	public Animal(String ten, String moTa) {
		super();
		this.ten = ten;
		this.moTa = moTa;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

}
